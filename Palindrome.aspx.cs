﻿using System;
using System.Web;
using System.Web.UI;
namespace assignmentbonus
{
    public partial class Palindrome : System.Web.UI.Page
    {



        protected void isPalindrome(object sender, EventArgs e)
        {
            if (!Page.IsValid)
            {
                return;
            }

           
            string userInput = userInputString.Text.ToString().ToLower();
           
            userInput = userInput.Replace(" ",string.Empty);
            int i = 0;
            int j = userInput.Length-1;
            Boolean flag = true;
            while(j>i)
            {
                if (userInput[i] != userInput[j])
                { flag = false; }
                i++;
                j--;

            }

             if (flag == false)
             { output.InnerHtml = "The string "+userInputString+" is  not palindrome"; }
             else
             { output.InnerHtml = "The string " + userInputString + "is  palindrome"; }


        }


        }

}
