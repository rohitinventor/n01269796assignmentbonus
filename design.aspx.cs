﻿using System;
using System.Web;
using System.Web.UI;
namespace assignmentbonus
{
    public partial class design : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        { }

        protected void Checkquadrant(object sender, EventArgs e)
        {
            if (!Page.IsValid)
            {
                return;
            }

            int xAxis = int.Parse(xinput.Text);
            int yAxis = int.Parse(yinput.Text);
            if (xAxis > 0 && yAxis > 0)
            { summary.InnerHtml = "The point ("+ xAxis +","+ yAxis + ") is in Quadrant I"; }

            else if (xAxis > 0 && yAxis < 0)
            { summary.InnerHtml = "The point (" + xAxis + "," + yAxis + ") is in Quadrant II"; }

            else if (xAxis < 0 && yAxis > 0)
            { summary.InnerHtml = "The point (" + xAxis + "," + yAxis + ") is in Quadrant IV"; }

            else if (xAxis < 0 && yAxis < 0)
            { summary.InnerHtml = "The point (" + xAxis + "," + yAxis + ") is in Quadrant III"; }

           
        }


    }
}
