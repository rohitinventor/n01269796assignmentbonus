﻿<%@ Page Language="C#"  MasterPageFile="~/Site.Master" CodeBehind="Divisible_Sizzable.aspx.cs"  Inherits="assignmentbonus.Divisible_Sizzable" %>

<asp:Content id="body" ContentPlaceHolderID="mainContent" runat="server">
     <div class="jumbotron">
        
        <h2>
            Check Prime Number
        </h2>
         <br/>
    
    <label for="Sizzableinput" >Enter A Number</label>
            <asp:TextBox runat="server" ID="userSizzableInput" ></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a value" ControlToValidate="userSizzableInput" ID="inputRequiredFieldValid"></asp:RequiredFieldValidator>
    
    
    <asp:RegularExpressionValidator ID="inputRegularExpressionValidator" runat="server"
      ValidationExpression="^\d+$" ControlToValidate="userSizzableInput" ErrorMessage="Invalid Format,Please enter a number">
    </asp:RegularExpressionValidator>    
    <br/>
    
    <asp:Button runat="server" ID="myButton"  OnClick="isPrime" Text="Submit"/>
    
  
    
    <div id="result" runat="server"></div>
        
        </div>
   
    </asp:Content>