﻿<%@ Page Language="C#"  MasterPageFile="~/Site.Master" CodeBehind="design.aspx.cs"  Inherits="assignmentbonus.design" %>


<asp:Content id="body" ContentPlaceHolderID="mainContent" runat="server">
    
     <div class="jumbotron">
        
        <h2>
            Check Quadrant
        </h2>
          <br />
      
    
    <label for="xinput">Enter X Axis</label>
            <asp:TextBox runat="server" ID="xinput" ></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a value" ControlToValidate="xinput" 
                                        ID="xinputRequiredFieldValid"></asp:RequiredFieldValidator>
    
    
    <asp:RegularExpressionValidator ID="xinputRegularExpressionValidator" runat="server"
      ValidationExpression="^[-+]?\d*[1-9]\d*\.?[0]*$" ControlToValidate="xinput" ErrorMessage="Invalid Format">
        
    </asp:RegularExpressionValidator>

         
    <br />
    
     <label for="yinput">Enter Y Axis</label>
<asp:TextBox runat="server" ID="yinput" ></asp:TextBox>
<asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a value" ControlToValidate="yinput" 
ID="yinputRequiredFieldValid"></asp:RequiredFieldValidator>
<asp:RegularExpressionValidator   ID="yinputRegularExpressionValidator" runat="server"
ValidationExpression="^[-+]?\d*[1-9]\d*\.?[0]*$" ControlToValidate="yinput" ErrorMessage="Invalid Format">

</asp:RegularExpressionValidator>
    <br/>

<asp:Button runat="server" ID="myButton"  OnClick="Checkquadrant" Text="Submit"/>

 
    
    <div id="summary" runat="server"></div>
          </div>
   </asp:Content> 