﻿<%@ Page Language="C#"  MasterPageFile="~/Site.Master" CodeBehind="Palindrome.aspx.cs"  Inherits="assignmentbonus.Palindrome" %>

<asp:Content id="body" ContentPlaceHolderID="mainContent" runat="server">
    
      <div class="jumbotron">
        
        <h2>
            Check Palindrome
        </h2>
       <br/>
    
    <label for="userInputString">Enter a String </label>
            <asp:TextBox runat="server" ID="userInputString" ></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a value" ControlToValidate="userInputString" ID="inputRequiredFieldValid"></asp:RequiredFieldValidator>
            <br />
    <asp:Button runat="server" ID="myButton"  OnClick="isPalindrome" Text="Submit"/>
    
  
    
    <div id="output" runat="server"></div>
         </div>
   </asp:Content> 